<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ホーム画面</title>
</head>
<body>
	<div class="profile">
		<div class="name">
			<h2>
				<c:out value="${loginUser.name}" />
				さんようこそ
			</h2>
		</div>
	</div>


	<a href="newpost.jsp">新規投稿</a>
	<br />
	<a href="management.jsp">登録情報の管理</a>
	<br />

	<div class="messages">
		<c:forEach items="${messages}" var="message">
			<div class="message">
				<div class="account-name">
					<span class="account"><c:out value="${message.account}" /></span>
					<span class="name"><c:out value="${message.name}" /></span>
				</div>
				<div class="text">
					<c:out value="${message.text}" />
				</div>
				<div class="date">
					<fmt:formatDate value="${message.createdDate}"
						pattern="yyyy/MM/dd HH:mm:ss" />
					<br />
				</div>
			</div>
		</c:forEach>
		<div class="comments">
			<form action="newComment" method="post">
				コメントする<br />

				<textarea name="text" cols="100" rows="2" class="tweet-box"></textarea>
				<input type="submit" value="送信" /> <br /> <br />


				<c:forEach items="${comments}" var="comment">
					
						<div class="account-name">
							<span class="account"><c:out value="${comment.account}" /></span>
							<span class="name"><c:out value="${comment.name}" /></span>
						</div>
						<div class="text">
							<c:out value="${comment.text}" />
						</div>
						<div class="date">
							<fmt:formatDate value="${comment.createdDate}"
								pattern="yyyy/MM/dd HH:mm:ss" />
							<br />
						</div>
				</c:forEach>
			</form>
		</div>
		</div>


		<c:if test="${ not empty loginUser }">

			<a href="logout">ログアウト</a>
		</c:if>

		<br /> <a href="login.jsp">戻る</a>
</body>
</html>